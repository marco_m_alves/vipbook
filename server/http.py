#coding: utf-8
import urllib2
import logging
from google.appengine.ext import deferred 


def get(url):
	logging.info('getting: ' + url)
	return urllib2.urlopen(url).read()

def get_via_queue(url):
	deferred.defer(get, url, _queue='poststore')

def post(url, body):
	logging.info('posting to: ' + url)
	return urllib2.urlopen(url, body).read()

def post_via_queue(url, body):
	deferred.defer(post, url, body, _queue='poststore')	