#coding: utf-8
pages = {
	u'Cláudia Vieira': 'ClaudiaVieira.Oficial',
	u'Diana Chaves': 'dianachaves.oficial',
	u'Rita Pereira': 'RitaPereira.pt',
	u'João Manzarra': 'JManzarra',
	u'Jessica Athayde': '275465999169172', # last
	u'Diogo Morgado': 'On.DiogoMorgado',
	u'Nuno Markl': 'nunomarkl',
	u'César Mourão': '314436851984162',
	u'Cristiano Ronaldo': 'Cristiano',
	u'Cristina Ferreira': 'cristinaferreiratvi',
	u'Manuel Luís Goucha': 'manuelluisgouchatvi'
}

# fields to be retrieved when fetching from facebook
fields = ['id','from','message','created_time','updated_time','picture','source','link', 'object_id']


# Categories
Humor = u'Humoristas'
Actor = u'Actores'
Presenter = u'Apresentadores'
Commenter = u'Comentadores'
Musician = u'Músico'
Model = u'Modelos'
Sports = u'Desportistas'
Decorator = u'Decoradores'

page_list = [
	{ 'readable_name': u'Cláudia Vieira', 'route': 'ClaudiaVieira.Oficial', 'categories': [Actor] },
	{ 'readable_name': u'Diana Chaves', 'route': 'dianachaves.oficial', 'categories': [Actor] },
	{ 'readable_name': u'Rita Pereira', 'route': 'RitaPereira.pt', 'categories': [Actor] },
	{ 'readable_name': u'João Manzarra', 'route': 'JManzarra', 'categories': [Presenter] },
	{ 'readable_name': u'Jessica Athayde', 'route': '275465999169172', 'categories': [Actor] }, 
	{ 'readable_name': u'Diogo Morgado', 'route': 'On.DiogoMorgado', 'categories': [Actor] },
	{ 'readable_name': u'Nuno Markl', 'route': 'nunomarkl', 'categories': [Humor] },
	{ 'readable_name': u'César Mourão', 'route': '314436851984162', 'categories': [Humor] },
	{ 'readable_name': u'Cristiano Ronaldo', 'route': 'Cristiano', 'categories': [Sports] },
	{ 'readable_name': u'Cristina Ferreira', 'route': 'cristinaferreiratvi', 'categories': [Presenter] },
	{ 'readable_name': u'Manuel Luís Goucha', 'route': 'manuelluisgouchatvi', 'categories': [Presenter] },
	{ 'readable_name': u'Andreia Rodrigues', 'route': '184189154973560', 'categories': [Presenter] },
	{ 'readable_name': u'Ana Antunes', 'route': 'ana.antunes.homestyling', 'categories': [Decorator] },
	{ 'readable_name': u'Rita Glória', 'route': 'ritagloriainteriordesign', 'categories': [Decorator] }
]

def get_page_routes():
	return [entry.get('route') for entry in page_list]

def get_categories_for_route(route):
	for entry in page_list:
		if entry.get('route') == route:
			return entry.get('categories')