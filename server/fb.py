
#coding: utf-8
import vip

APP_ID = '132276883573923'
APP_SECRET = '8072a0d20d272aa77b111246954176da'


def posts_from_page_url(params={}):
	params['access_token'] = APP_ID + '|' + APP_SECRET
	params['fields'] = ','.join(vip.fields)
	url = "https://graph.facebook.com/%(page)s/posts?access_token=%(access_token)s&fields=%(fields)s" % params
	if params.get('since'):
		url += '&since=%(since)s' % params
	return url

def page_url(params={}):
	return 'https://graph.facebook.com/%(page)s' % params
	
if __name__ == "__main__":
	pass