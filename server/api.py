#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
from google.appengine.ext import ndb
import services
import logging
import json
import vip
import http
import fb
from models import Post, Page

class MainHandler(webapp2.RequestHandler):

    def write(self, stuff):
         self.response.out.write(stuff)


    def get(self):
        self.write('hey!');
       



# Posts

class FBPagePostsHandler(webapp2.RequestHandler):

    def write(self, stuff, log=False):
        if log:
         logging.info(stuff)
        self.response.out.write(stuff)

    def get(self, page):
        next_url = self.request.get('next_url')
        since = self.request.get('since')
        json_data = services.fetch_new_posts_for_page(page, since)

        if next_url:
            for entry in json.loads(json_data).get('data'):
                post_json = json.dumps(entry)
                http.post_via_queue(next_url, post_json)

        self.write(json_data)


class FBPostsHandler(webapp2.RequestHandler):

    def write(self, stuff):
        logging.info(stuff)
        self.response.out.write(stuff)

    def post(self, post_id=None):
        json_params = self.request.body
        if not post_id:
            params = json.loads(json_params)
            if Post.post_id_exists(params.get('id')):
                self.write('About to override post with: ' + json_params)
                existing = Post.get_one_by_fb_post_id(params.get('id'))
                existing.key.delete()
            else:
                self.write('About to store post: ' + json_params)
            new_post = Post.fromJson(json_params)
            # Associate with page cover source
            page = Page.get_by_page_id(new_post.from_id)
            if page:
                new_post.cover_source = page.cover_source
            # Associate with the page assigned categories
            if page.categories:
                new_post.categories = page.categories
            # Store post
            new_post.put()
            self.write('Stored post with facebook id: ' + new_post.fb_post_id)
            
        else:
            self.write('A post with fb id ' + post_id + ' was given. Trying to update?')

    def get(self, post_id=None):
        cursor = self.request.get('cursor')
        categories = self.request.get('categories')
        
        if categories:
            categories = categories.split(',')

        if not post_id:
            logging.info(categories)
            posts, next_curs, more = Post.get_last(20, categories, cursor)
            
            # Update fields that might not have been stored with the post
            for post in posts:
                page = Page.get_by_page_id(post.from_id)
                # Update the cover source if it's not already defined
                if not post.cover_source:
                    post.cover_source = page.cover_source
                # Update categories if not already present
                if not post.categories:
                    logging.info(post.from_id)
                    logging.info(page.categories)
                    if page.categories:
                        post.categories = page.categories
                logging.info(post)

            posts = [post.to_dict() for post in posts]
            response = {'posts': posts, 'next_curs': next_curs.urlsafe() if next_curs else None, 'more': more}
            self.write(json.dumps(response))
        



class FBPagePostsCronHandler(webapp2.RequestHandler):

    def get(self):
        host = self.request.host
        since = services.current_date_as_string()
        pages = vip.get_page_routes()
        store_posts_url = 'http://' + host + '/api/posts'
        for page in pages:
            services.get_new_posts_for_page(host, page, since, store_posts_url)



# Pages

class FBPagesHandler(webapp2.RequestHandler):

    def write(self, stuff):
        logging.info(stuff)
        self.response.out.write(stuff)

    def get(self, page=None):
        next_url = self.request.get('next_url')

        if page:
            page_url = fb.page_url({'page': page})
            logging.info('pageurl ' + page_url)
            page_json_data = http.get(page_url)

            # add the route to the page json data
            data = json.loads(page_json_data)
            data['route'] = page
            page_json_data = json.dumps(data)

            if next_url:
                http.post_via_queue(next_url, page_json_data)

            self.write(page_json_data)

        else:
            pages = Page.query().fetch(20)
            pages = [page.to_dict() for page in pages]
            response = {'pages': pages}
            self.write(json.dumps(response))

    def post(self, page=None):
        json_params = self.request.body
        params = json.loads(json_params)
        if not page:
            fb_page_id =  params.get('id')
            # Check if page already exists
            existing = Page.get_by_page_id(fb_page_id)
            if existing:
                self.write('About to override page with: ' + json_params)
                existing.key.delete()
            else:
                self.write('About to store page with: ' + json_params)
            # Creat page
            new_page = Page.fromJson(json_params)
            # Attach categories
            route = params.get('route')
            categories = vip.get_categories_for_route(route) 
            if categories:
                new_page.categories = categories
            # Store page    
            new_page.put()
            self.write('Stored page with facebook id: ' + new_page.fb_page_id)

        else:
            pass


class FBPagesCronHandler(webapp2.RequestHandler):

    def get(self):
        host = self.request.host
        pages = vip.get_page_routes()
        store_page_url = 'http://' + host + '/api/pages'
        for page in pages:
            services.get_page_data(host, page, store_page_url)



# Repair

class RepairHandler(webapp2.RequestHandler):
   
    def get(self):
        posts = Post.query().order(-Post.created_time)
        for post in posts:
            page = Page.get_by_page_id(post.from_id)
            post.cover_source = page.cover_source
            post.categories = page.categories
        ndb.put_multi(posts)


class UniquePostHandler(webapp2.RequestHandler):
    
    def get(self, fb_post_id=None):
        if fb_post_id:
            posts = Post.get_all_by_fb_post_id(fb_post_id)
            if len(posts[1:]) > 0:
                ndb.delete_multi(posts)
        else:
            host = self.request.host
            posts = Post.query().order(-Post.created_time).fetch(500)
            for post in posts:
                services.ensure_that_post_is_unique_via_queue(host, post.fb_post_id)






app = webapp2.WSGIApplication([
                                ('/api/', MainHandler),
                                ('/api/pages/cron', FBPagesCronHandler),
                                ('/api/pages/(\w[\w|\.]+)', FBPagesHandler),
                                ('/api/pages', FBPagesHandler),
                                ('/api/pages/(\w[\w|\.]+)/posts', FBPagePostsHandler),
                                ('/api/posts/cron', FBPagePostsCronHandler),
                                ('/api/posts', FBPostsHandler),
                                ('/api/posts/([\d|_]*)|', FBPostsHandler),
                                ('/api/posts/([\d|_]*)|/unique', UniquePostHandler),
                                ('/api/repair/posts/fields', RepairHandler),
                                ('/api/repair/posts/unique', UniquePostHandler)
                                ],
                              debug=True)
