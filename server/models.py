from google.appengine.ext import ndb
from google.appengine.datastore.datastore_query import Cursor
import json

class Post(ndb.Model):
	fb_post_id = ndb.StringProperty(required=True)
	from_id = ndb.StringProperty(required=True)
	from_name = ndb.StringProperty(required=True)
	message = ndb.TextProperty()
	created_time = ndb.StringProperty(required=True)
	updated_time = ndb.StringProperty(required=True)
	picture = ndb.StringProperty()
	link = ndb.StringProperty()
	source = ndb.StringProperty()
	object_id = ndb.StringProperty()
	cover_source = ndb.StringProperty()
	caption = ndb.StringProperty()
	categories = ndb.StringProperty(repeated=True)

	@classmethod
	def fromJson(cls, post_json):
		obj = json.loads(post_json)
		post = cls.fromDict(obj)
		return post

	@classmethod
	def fromDict(cls, obj):
		post = Post(
			fb_post_id=obj.get('id'),
			from_id=obj.get('from').get('id'),
			from_name=obj.get('from').get('name'),
			message=obj.get('message'),
			created_time=obj.get('created_time'),
			updated_time=obj.get('updated_time'),
			picture=obj.get('picture'),
			link=obj.get('link'),
			source=obj.get('source'),
			object_id=obj.get('object_id'),
			cover_source=obj.get('cover_source'),
			caption=obj.get('caption'),
			categories=obj.get('categories') if obj.get('categories') else []
			)
		return post

	@classmethod
	def post_id_exists(cls, post_id=None):
		return cls.query(cls.fb_post_id == post_id).get()

	@classmethod
	def get_one_by_fb_post_id(cls, fb_post_id):
		return cls.query(cls.fb_post_id == fb_post_id).get()

	@classmethod
	def get_all_by_fb_post_id(cls, fb_post_id):
		qry = cls.query(cls.fb_post_id == fb_post_id).order(-cls.key)
		return list(qry)

	@classmethod
	def get_last(cls, count=20, categories=None, cursor=None):
		curs = Cursor(urlsafe=cursor)
		qry = cls.query()
		if categories:
			qry = qry.filter(cls.categories.IN(categories))
		return qry.order(-cls.created_time, -cls.key).fetch_page(20, start_cursor=curs)



class Page(ndb.Model):
	fb_page_id = ndb.StringProperty(required=True)
	name = ndb.StringProperty(required=True)
	route = ndb.StringProperty(required=True)
	likes = ndb.StringProperty(required=True)
	talking_about_count = ndb.StringProperty(required=True)
	cover_source = ndb.StringProperty(required=True)
	categories = ndb.StringProperty(repeated=True)
	
	@classmethod
	def fromDict(cls, obj):
		page = Page(
			fb_page_id=obj.get('id'),
			name=obj.get('name'),
			likes=str(obj.get('likes')),
			talking_about_count=str(obj.get('talking_about_count')),
			cover_source=obj.get('cover').get('source'),
			route=obj.get('route')
			)
		return page

	@classmethod
	def fromJson(cls, json_data):
		obj = json.loads(json_data)
		return cls.fromDict(obj)

	@classmethod
	def get_by_page_id(cls, page_id):
		return Page.query(cls.fb_page_id == page_id).get()

	@classmethod
	def get_by_page_route(cls, route):
		return Page.query(cls.route == route).get()

	@classmethod
	def get_last(cls, count=20, cursor=None):
		curs = Cursor(urlsafe=cursor)
		return cls.query().fetch_page(20, start_cursor=curs)


