#coding: utf-8

import logging
import http
import fb
from datetime import datetime
from google.appengine.ext import deferred


# Url fecthing servicess

def fetch_new_posts_for_page(page, since=None):
	url = fb.posts_from_page_url({'page': page, 'since': since})
	logging.info('retrieving posts for: ' + page + ' @ ' + url)
	data = http.get(url)
	logging.info('done!')
	return data


def fetch_new_posts_for_page_via_queue(page):
	deferred.defer(fetch_new_posts_for_page, page, _queue='poststore')


def fetch_new_posts_for_pages(pages):
	for page in pages:
		fetch_new_posts_for_page_via_queue(page)
	return len(pages)
	

def get_new_posts_for_page(host, page, since=None, next_url=None):
	url = 'http://' + host + '/api/pages/' + page + '/posts'
	if since:
		url+='?since=' + since
	if next_url:
		url += '&next_url=' + next_url
	http.get_via_queue(url)

def store_post(host, post_json):
	url = 'http://' + host + '/api/posts'
	response = http.post(url, post_json)
	return response

def store_post_via_queue(host, post_json):
	deferred.defer(store_post, host, post_json, _queue='poststore')

def get_page_data(host, page, next_url):
	url = 'http://' + host + '/api/pages/' + page 
	if next_url:
		url += '?next_url=' + next_url
	data = http.get_via_queue(url)
	return data

def ensure_that_post_is_unique_via_queue(host, post_id):
	url = 'http://' + host + '/api/posts/' + post_id + '/unique'
	http.get_via_queue(url)

# Date related services

def get_date_as_string(date):
	return date.strftime('%Y-%m-%d')

def current_date_as_string():
	return get_date_as_string(datetime.today())