angular.module('VipBook', ['ui.bootstrap']).
config(function($routeProvider) {
  $routeProvider.
    when('/', { controller: 'LandingPageCtrl', templateUrl: 'landing-page.html' }).
    when('/newsfeed/:category', { controller: 'NewsFeedCtrl', templateUrl: 'news-feed.html' }).
    when('/posts/:id', { controller: 'PostViewCtrl', templateUrl: 'post-view.html' }).
    when('/preferences', { controller: 'PreferencesCtrl', templateUrl: 'preferences.html' }).
    otherwise('/', { controller: 'LandingPageCtrl', templateUrl: 'landing-page.html' });
});
