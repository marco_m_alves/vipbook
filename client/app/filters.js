angular.module('VipBook').
filter('big', function(){
  return function(imageUrl){
    var re1 = /_s\.jpg$/,
        re2 = /_s\.png$/,
        parts;

    if (!imageUrl) return;
    
    if (imageUrl.match(re1)) {
      
      parts = imageUrl.split(re1);
      return parts[0] + '_n.jpg';  
    
    } else if (imageUrl.match(re2)) {

      parts = imageUrl.split(re2);
      return parts[0] + '_n.png';
    
    } else {

      return imageUrl;
    
    }
  };
}).
filter('html5VideoUrl', function() {
  var sapoRe = /http:\/\/imgs.sapo.pt\/sapovideo\/swf\/flvplayer-sapo.swf\?/,
      sapoHtml5Url = 'http://videos.sapo.pt/playhtml?',
      ytRe = 'http://www.youtube.com/v/',
      ytHtml5url = 'http://www.youtube.com/embed/';

  return function(url) {
    if (!url) return;

    if (url.match(sapoRe)) {
      return url.replace(sapoRe, sapoHtml5Url);
    }

    if (url.match(ytRe)) {
      return url.replace(ytRe, ytHtml5url);
    } 

    return url;
  };
}).
filter('noAutoPlay', function() {
  var ytRe = /autoplay=1/gi;
  var sapoRe = /autostart=1/gi;

  return function(videoUrl) {
    if (!videoUrl) return;

    if (videoUrl.match(ytRe)) {

      return videoUrl.replace(ytRe, 'autoplay=0');

    } else if (videoUrl.match(sapoRe)){
      
      return videoUrl.replace(sapoRe, 'autostart=0'); 

    } 
    
    return videoUrl;
  };
}).
filter('blockOn', function(){
  return function(value, trigger){
    return trigger? value: undefined;
  };
}).
filter('keys', function() {
  return function(obj){
    if (obj) return Object.keys(obj);
    else return [];
  };
}).
filter('unique', function(Index){
  return function(array, field){
    var index = new Index();

    angular.forEach(array, function(item) {
      Index.add(item[field], item);
    });
  };
});