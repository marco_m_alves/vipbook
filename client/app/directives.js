angular.module('VipBook').
directive('scrollImgOnEnter', function(){
  return function(scope, elm, attrs){
    var $container, $img, 
        defMarginTop, defMarginLeft, 
        animation, 
        shorter, thinner;
    
    $container = elm;
    $img = elm.find('img');
    
    
    $img.mouseenter(function(){
      shorter = $img.height() <= $container.height();
      thinner = $img.width() <= $container.width();
      
      console.log(shorter, thinner);
      
      if (shorter && thinner) return;

      animation = {};

      if (!shorter) {
        defMarginTop = $img.css('margin-top');
        animation['margin-top'] = $container.height() - $img.height();
      }
      
      if (!thinner) {
        defMarginLeft = $img.css('margin-left');
        animation['margin-left'] = $container.width() - $img.width();
      }
      
      $img.animate(animation);
    });


    
    $img.mouseleave(function(){

      shorter = $img.height() <= $container.height();
      thinner = $img.width() <= $container.width();
      
      if (shorter && thinner) return;

      animation = {};

      if (!shorter) {
        animation['margin-top'] = defMarginTop;
      }
      
      if (!thinner) {
        animation['margin-left'] = defMarginLeft;
      }

      $img.animate(animation);
    });
    
  };
}).
directive('expandHeight', function($timeout){
  return function(scope, elm, attrs){
    var $container, $img, shorter;
    
    $container = elm;
    $img = elm.find('img');
    
    function applyStyles(){
        shorter = $img.height() <= $container.height();

        if (shorter) {
          $img.css('height', '100%');
          $img.css('max-width', 'none');
        }
    }

    $img.load(applyStyles);
  };
}).
directive('vCenter', function(){
  return function(scope, elm, attrs){
    
    function center(){
      elm.css({
        position:'absolute',
        left: ($(window).width() - elm.outerWidth())/2,
        top: ($(window).height() - elm.outerHeight())/2,
        width: '100%',
      });
    }
    
    $(window).resize(center);
 
    // To initially run the function:
    center();
  };
}).
directive('pillSelect', function(){
  var template;
  
  template = '<span ng-repeat="option in options">' +
                  '<a class="button button-pill button-flat button-small" ng-class="{ \'button-flat-royal\': isSelected($index) }" ng-click="select($index)">' + 
                    '{{option}}' + 
                  '</a>&nbsp;' +
              '</span>';
  
  
  function link(scope, elm, attrs){
    var selected;
  
    scope.select = function(index){
      selected = index;
      scope.selected = scope.options[index];
      if (scope.onSelection) {
        scope.onSelection({$selected: scope.selected});
      }
    };
  
    scope.isSelected = function(index){
      return selected === index;
    };


    function updateSeleted(){
      if (scope.options) selected = scope.options.indexOf(scope.selected);
    }

    scope.$watch('selected', updateSeleted);
    scope.$watch('options', updateSeleted);
  }
  
  return {
    restrict: 'E',
    template: template,
    scope: { options: '=', selected: '=', onSelection: '&' },
    link: link
  };
})
.directive('videoPlayerIcon', function(){
  return function(scope, elm, attrs){
    
    function getSize(icon){
      return {
        left: (elm.width() - icon.width()) / 2,
        top: (elm.height() - icon.height()) / 2
      };
    }
    
    if (scope.$eval(attrs.videoPlayerIcon)) {
      var template = '<span style="position: absolute;"><i class="icon-play-circle"></i></span>';
      var icon = angular.element(template);
      icon.find('i').css('font-size', elm.width() / 4);
      elm.append(icon);
      icon.css(getSize(icon));

      icon.click(function(){
        scope.$apply(function(){
          if (attrs.onIconClick) scope.$eval(attrs.onIconClick);
        });
      });
    }
  };
});