angular.module('VipBook').
service('Post', function ($http, $filter) {

	var cursor, last_categories;
	
	this.posts = [];
	this.visiblePosts = [];
	this.loading = false;
	this.loadedOnce = false;

	
	// Buils the url to fetch the posts from
	function getUrl(categories, cursor) {
		var url;
		url = '/api/posts';
		url += (categories || cursor)? '?' : '';
		url += categories? '&categories=' + categories.join(',') : '';
		url += cursor? '&cursor=' + cursor : '';
		return url;
	}


	// Filters the post list by a search term
	this.updateVisiblePosts =  function (search) {
		var filter = $filter('filter');
		this.visiblePosts = filter(this.posts, search);
	}

 	
 	// Fetches posts
  	this.getPosts = function(categories, search, callback) {
    	var self, url, changedCategory;
    	
    	self = this;
    	changedCategory = categories !== last_categories;

    	if (changedCategory) {
    		self.posts = [];
    		cursor = undefined;
    	}

    	url = getUrl(categories, cursor);
    	
    	this.loading = true;

    	$http.get(url).success(function(resp){
			cursor = resp.next_curs;
      		self.posts = self.posts.concat(resp.posts);
			self.updateVisiblePosts(search);
      		self.loading = false;
      		self.loadedOnce = true;
      		if (callback) callback();
    	});
    };
})
.service('Page', function($http, Index) {
	var cursor;

	this.pages = [];
	this.visiblePages = [];
	this.categoryIndex =  undefined;

	function getUrl (cursor) {
		var baseUrl, url;
		baseUrl = '/api/pages';
		url = baseUrl + (cursor? '?cursor='+cursor : '');
		return url;
	}

	function getCategories(pages){
		var index = new Index();

		angular.forEach(pages, function(page){
			angular.forEach(page.categories, function(category) {
				index.add(category, page);
			}); 
		});

		return index._index;
	}

	this.getPages = function(search, callback) {
    	var self = this;
    	this.loading = true;
    	var url = getUrl(cursor);

    	$http.get(url).success(function(resp){
			cursor = resp.next_curs;
      		self.pages = self.pages.concat(resp.pages);
      		self.categoryIndex = getCategories(resp.pages);
      		self.loading = false;
      		self.loadedOnce = true;
      		if (callback) callback();
    	});
    };

}).
constant('Categories', {
	All: 'Todos',
	Actors: 'Actores',
	Comedians: 'Humoristas',
	Presenters: 'Apresentadores',
	Decorators: 'Decoradores',
	Sports: 'Desportistas'
}).
service('Category', function(Categories, Page, Index) {

	// helper method to build the list
	this.buildList = function () {
		var _list, index, first;
		_list = _.values(Categories).sort();
		index = _list.indexOf(Categories.All);
		first = _list.splice(index, 1);
		_list.unshift(first[0]);
		return _list;
	};

	this.list = this.buildList();

	this.selected = undefined;

	this.queryParameter = function(category) {
		category = category || this.selected || 'Todos';
		if (category === 'Todos') return undefined;
		return category;
	};

	this.getPagesByCategory = function() {
		var index = new Index();

		angular.forEach(Page.pages, function(page) {
			angular.forEach(page.categories, function(category) {
				index.add(category, page);
			});
		});

		console.log(index._index);

		return index._index;
	};
}).
factory('Index', function(){
  
	function Index(){
	    this._index = {};
	}

	// Convenience for associating a list with a given key
	Index.prototype.add = function(key, content){
	    var noPreviousContent = !this._index[key] || !angular.isArray(this._index[key]);
	    
	    if (noPreviousContent) {
	      this.set(key,[content]);  
	    } else {
	      this._index[key].push(content);
	    }
	};

	// For associating any value type with a key
	Index.prototype.set = function(key, content){
	      this._index[key] = content;
	};


	Index.prototype.get = function(key){
	    return this._index[key];
	};

	Index.prototype.keys = function() {
	    return Object.keys(this._index);
	};

	return Index;
});