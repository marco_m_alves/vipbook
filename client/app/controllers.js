angular.module('VipBook').
controller('MainCtrl', function($scope) {
  
  // nothing...
  
}).
controller('LandingPageCtrl', function ($scope, $location, Post, Page, Category) {

    
    Post.getPosts(Category.queryParameter(), $scope.search, function() {
        $location.path('/newsfeed/' + 'Todos');
    });
    
}).
controller('NewsFeedCtrl', function ($scope, $location, $routeParams, Post, Page, Category, Categories) {

    if (!Post.loadedOnce) {
        $location.path('/');
        return;
    }

    $scope.Post = Post;
    $scope.Page = Page;
    $scope.Category = Category;
    Category.selected = $routeParams.category || Categories.All;
    
    
    Post.getPosts([Category.queryParameter()], $scope.search);
    

    $scope.more = function () {
        var search =  $scope.search;
        Post.getPosts(search);
    };

    $scope.showCategory = function(selectedCategory){
        $location.path('/newsfeed/' + selectedCategory);
    };

    $scope.showPost = function (post) {
        $location.path('/posts/' + post.fb_post_id);
    };

    $scope.$watch('search', function() {
        var search =  $scope.search;
        Post.updateVisiblePosts(search);
    }, true);

}).
controller('PostViewCtrl', function($scope, $location, $routeParams, Post) {

    if (!Post.loadedOnce) {
        $location.path('/');
        return;
    }

    angular.forEach(Post.posts, function(post) {
        if (post.fb_post_id === $routeParams.id) {
            $scope.post = post;
        }
    });

    $scope.goBack = function() {
       history.go(-1);
    };


    $scope.showOther = function(event) {
        var right = event.clientX > $(window).width() * 2/3,
            left = event.clientX < $(window).width() * 1/3;

        if (right) showNext();
        else if (left) showPrevious();
    };

    function showNext() {
        var index, nextPost;
        
        index = Post.posts.indexOf($scope.post);

        if (index < Post.posts.length - 1) {
            nextPost = Post.posts[index + 1];
            $location.path('/posts/' + nextPost.fb_post_id);
        }
    };

    function showPrevious() {
        var index, nextPost;
        
        index = Post.posts.indexOf($scope.post);

        if (index > 0) {
            nextPost = Post.posts[index - 1];
            $location.path('/posts/' + nextPost.fb_post_id);
        }
    };
}).
controller('PreferencesCtrl', function ($scope, Categories, Category, Page) {

    function updatePages (selected) {
        $scope.pages = Page.pages;
        // $scope.pages =  Page.pages.filter(function(page) {
        //     if (!selected) return true;
        //     if (page.categories) return page.categories.indexOf(selected) >= 0;
        // });
    }

    $scope.Page = Page;
    $scope.Category = Category;
    $scope.Categories = Categories;

    $scope.showCategory = function(selected) {
        updatePages(selected);        
    };

    updatePages(Category.selected || Categories.All);

    Page.getPages(undefined, function() {
        updatePages(Categories.All);
        $scope.pagesByCategory = Category.getPagesByCategory();
    });

});