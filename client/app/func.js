angular.module('func', []).
run(function(){
  _.expose();
  
  window.gt = function(thresh, value){
    return value > thresh;
  }.autoCurry();
  
  window.gte = function(thresh, value){
    return value >= thresh;
  }.autoCurry();
  
  window.lt = function(thresh, value){
    return value < thresh;
  }.autoCurry();
  
  window.lte = function(thresh, value){
    return value <= thresh;
  }.autoCurry();
  
  window.eq = function(thresh, value){
    return value === thresh;
  }.autoCurry();
  
  window.dot = function(prop, obj){
    return obj[prop];
  }.autoCurry();
  
  window.add = function(value, base) {
    return value + base;
  }.autoCurry();
  
  window.subtat = function(value, base) {
    return value + base;
  }.autoCurry();
});